 #include <Servo.h>

 


  // put your setup code here, to run once:
// Pinii motor 1
#define mpin00 5
#define mpin01 6
// Pinii motor 2
#define mpin10 3
#define mpin11 11
#define DEBUG true
const int echoPin=2;
const int trigPin=12;

long duration;
int distance=0;
int distance_0=0;
int distance_90=0;
int distance_180=0;
bool startRotation=false;
bool startRobot=false;
int angle=90;
Servo srv;


void setup() {

  Serial.begin(115200);
  sendData("AT+RST\r\n", 2000, false); // resetare modul
  sendData("AT+CWSAP=\"Kos_George\",\"12345678\",5,3\r\n", 2000, DEBUG); // citeste informatia SSID (nume retea)
  sendData("AT+CWMODE=2\r\n", 1000, false); // configurare ca access point
  sendData("AT+CIFSR\r\n", 1000, DEBUG); // citeste adresa IP
  sendData("AT+CWSAP?\r\n", 2000, DEBUG); // citeste informatia SSID (nume retea)
  sendData("AT+CIPMUX=1\r\n", 1000, false); // configurare conexiuni multiple
  sendData("AT+CIPSERVER=1,80\r\n", 1000, false); // pornire server pe port 80
   // configurarea pinilor motor ca iesire, initial valoare 0
 digitalWrite(mpin00, 0);
 digitalWrite(mpin01, 0);
 digitalWrite(mpin10, 0);
 digitalWrite(mpin11, 0);
 pinMode (mpin00, OUTPUT);
 pinMode (mpin01, OUTPUT);
 pinMode (mpin10, OUTPUT);
 pinMode (mpin11, OUTPUT);
 pinMode(13, OUTPUT);
 pinMode(trigPin,OUTPUT);
 pinMode(echoPin,INPUT);
 playWithServo(8,90);

}

void StartMotor (int m1, int m2, int forward, int speed)
{

 if (speed==0) // oprire
 {
 digitalWrite(m1, 0); 
 digitalWrite(m2, 0);
 }
 else
 {
 if (forward)
 {
 digitalWrite(m2, 0);
 analogWrite (m1, speed); // folosire PWM
 }
 else
 {
 digitalWrite(m1, 0);
 analogWrite(m2, speed);
 }
 }
}

void delayStopped(int ms)
{
 StartMotor (mpin00, mpin01, 0, 0);
 StartMotor (mpin10, mpin11, 0, 0);
 delay(ms);
}

void playWithServo(int pin,int angle)
{
 srv.attach(pin);
 srv.write(angle);
 delay(1000);
 srv.detach();
}

int calcDistance(){
      digitalWrite(trigPin,LOW);
      delayMicroseconds(2);
      digitalWrite(trigPin,HIGH);
      
      delayMicroseconds(10);
      duration=pulseIn(echoPin,HIGH);
      distance=duration* 0.034/2;

      return distance;
  
 }

void loop() {
 
if (Serial.available()) {
    if (Serial.find("+IPD,")) {
      delay(500);
      int connectionId = Serial.read() - 48; // functia read() returneaza valori zecimale ASCII
      // si caracterul ‘0’ are codul ASCII 48
      String webpage = "<h1>Hello World!</h1><a href=\"/l0\"><button>START</button></a>";
      String cipSend = "AT+CIPSEND=";
      cipSend += connectionId;
      cipSend += ",";
      webpage += "<a href=\"/l1\"><button>STOP</button></a>";
      cipSend += webpage.length();
      cipSend += "\r\n";
      sendData(cipSend, 100, DEBUG);
      sendData(webpage, 150, DEBUG);

      String closeCommand = "AT+CIPCLOSE=";
      closeCommand += connectionId; //se adauga identificatorul conexiunii
      closeCommand += "\r\n";
      sendData(closeCommand, 300, DEBUG);
    }
  }
 //if(startRobot){
  if(angle==90){
  
    distance_90=calcDistance();
  }
 
  if(distance_90<15){
    delayStopped(1);
    startRotation=true;
 
 }
  if(startRotation){
    if(angle==90){
  
      angle=0;
      playWithServo(8,angle);
      distance_0=calcDistance();

 }
    if(angle==0){
  
      angle=180;
      playWithServo(8,angle);
      
      distance_180=calcDistance();
  
 }

    if(angle==180){
      angle=90;
      playWithServo(8,angle);
      startRotation=false;
  
 }
}


 if(distance_0>=15 and distance_0>distance_180) {
      StartMotor (mpin00, mpin01, 0, 128);
      StartMotor (mpin10, mpin11, 1, 128);
      delay(500);
      distance_0=0;
      distance_180=0; 
}

 else if(distance_180>=15 and distance_180>=distance_0){
      
      StartMotor (mpin00, mpin01, 1, 128);
      StartMotor (mpin10, mpin11, 0, 128);
      delay(500);
      distance_0=0; 
      distance_180=0;
    }
 
      StartMotor (mpin00, mpin01, 0, 100);
      StartMotor (mpin10, mpin11, 0, 100); 
 

 }
 //else{
 // delayStopped(0);
// }
//}

 String sendData(String command, const int timeout, boolean debug) {
  String response = "";
  //Serial.println(command);

  Serial.print(command); // trimite comanda la esp8266
  long int time = millis();
  while ((time + timeout) > millis()) {
    while (Serial.available()) {
      char c = Serial.read(); // citeste caracter urmator
      response += c;
    }
  }
  if (response.indexOf("/l0") != -1) {
    startRobot=true;
  }
  if (response.indexOf("/l1") != -1) {
    startRobot=false;
  }
  return response;
}


